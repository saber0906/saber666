# 软件技术基础第四次作业

#### 介绍
实现一个简易命令行计算器程序。输入数字和算法后能进行四则运算加减乘除，计算结果保留到小数点后2位。
程序能处理用户的输入，判断异常。
程序支持可以由用户自行选择加、减、乘、除运算。


#### 运行结果
![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/172732_c02c4f62_9835559.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/172801_f673c0fc_9835559.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/172824_29e13789_9835559.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/172845_aa68d142_9835559.png "屏幕截图.png")
